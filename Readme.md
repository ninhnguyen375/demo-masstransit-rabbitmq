
### Detail:
#### Start two servers first.
1. APIs in collection TestMessageBus (click `run in posman` to import).
2. Run api `CatalogApi - Get all product` to check quantity of products.
3. Run api `OrderApi - New order` to make place an order.
4. Run api `CatalogApi - Get all product` to check quantity again.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/6bf54038093c3da13c59)