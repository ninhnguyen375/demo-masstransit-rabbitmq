namespace MessageTypes.CatalogService {
    public class ListProductMessage {
        public int Id { get; set; }
        public string Message { get; set; }
    }
}