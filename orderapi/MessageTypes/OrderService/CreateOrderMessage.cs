using System.Collections.Generic;
using orderapi.DTOs.EntityDTO;
using orderapi.Models;

namespace MessageTypes.OrderService {
    public class CreateOrderMessage {
        public int Id { get; set; }
        public double TotalPrice { get; set; }
        public virtual ICollection<OrderDetailDTO> OrderDetail { get; set; }
    }
}