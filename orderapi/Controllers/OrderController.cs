using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using orderapi.DTOs.EntityDTO;
using orderapi.DTOs.RequestDTO;
using MessageTypes.OrderService;
using orderapi.Models;

namespace orderapi.Controllers {
    [ApiController]
    [Route ("[controller]")]
    public class OrderController : ControllerBase {
        private readonly AppDbContext _context;
        private readonly IBus _bus;
        private readonly IMapper _mapper;
        public OrderController (AppDbContext context, IBus bus, IMapper mapper) {
            _context = context;
            _bus = bus;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult> GetOrders () {
            var orders = await _context.Orders.Include (o => o.OrderDetail).ToListAsync ();
            var mappedOrders = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderDTO>> (orders);
            return Ok (mappedOrders);
        }

        [HttpGet ("{id}")]
        public async Task<ActionResult> GetOrder (int id) {
            var order = await _context.Orders.FindAsync (id);
            await _context.OrderDetail.ToListAsync ();

            var mappedOrder = _mapper.Map<Order, OrderDTO> (order);
            return Ok (mappedOrder);
        }

        [HttpPost]
        public async Task<ActionResult> CreateOrder ([FromBody] CreateOrderDTO createOrderDTO) {
            double totalPrice = 0;
            var orders = await _context.Orders.ToListAsync ();
            var lastOrder = orders.Any () ? orders.Last () : null;
            int orderId = lastOrder == null ? 1 : lastOrder.Id + 1;

            // Add order first
            Order order = new Order { Id = orderId };
            await _context.Orders.AddAsync (order);

            List<OrderDetail> orderDetail = new List<OrderDetail> ();
            foreach (var cartItem in createOrderDTO.CartItems) {
                // Validate product Quantity
                var currentProduct = await _context.Products.FindAsync (cartItem.ProductId);
                if (currentProduct == null) {
                    return BadRequest (new {
                        errors = new {
                            message = "Product Id " + cartItem.ProductId + " not found"
                        }
                    });
                }
                int newQuantity = currentProduct.Quantity - cartItem.Quantity;
                if (newQuantity < 0) {
                    return BadRequest (new {
                        errors = new {
                            message = "Product " + currentProduct.Name + " is out of stock"
                        }
                    });
                } else {
                    currentProduct.Quantity = newQuantity;
                }

                orderDetail.Add (new OrderDetail {
                    OrderId = order.Id,
                        ProductId = currentProduct.Id,
                        ProductName = currentProduct.Name,
                        ProductPrice = currentProduct.Price,
                        Quantity = cartItem.Quantity,
                        TotalPrice = cartItem.Quantity * currentProduct.Price,
                });
                // Making total price
                totalPrice += cartItem.Quantity * currentProduct.Price;
            }
            // Apply totalPrice
            order.TotalPrice = totalPrice;
            // Add order detail
            await _context.OrderDetail.AddRangeAsync (orderDetail);

            // Almost done, save db changes
            await _context.SaveChangesAsync ();

            // Publish message type CreateOrderMessage
            CreateOrderMessage message = _mapper.Map<Order, CreateOrderMessage> (order);
            await _bus.Publish (message);

            return Ok (order);
        }
    }
}