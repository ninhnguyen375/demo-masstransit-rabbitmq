using AutoMapper;
using MessageTypes.OrderService;
using orderapi.DTOs.EntityDTO;
using orderapi.DTOs.RequestDTO;
using orderapi.Models;

namespace orderapi.Infrastructure {
    public class MappingProfile : Profile {
        public MappingProfile () {
            CreateMap<Order, OrderDTO> ();
            CreateMap<OrderDetail, OrderDetailDTO> ();
            CreateMap<Order, CreateOrderMessage> ();
        }
    }
}