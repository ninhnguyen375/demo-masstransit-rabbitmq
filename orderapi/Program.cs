using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using orderapi.Models;

namespace orderapi {
    public class Program {
        public static void Main (string[] args) {
            var host = CreateHostBuilder (args).Build ();

            using (var scope = host.Services.CreateScope ()) {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<AppDbContext> ();
                try {
                    context.Database.EnsureCreated ();

                    if (!context.Products.Any ()) {
                        context.Products.AddRange (
                            new Product { Id = 1, Name = "iPhone 1", Price = 10000, Quantity = 10 },
                            new Product { Id = 2, Name = "iPhone 2", Price = 20000, Quantity = 10 }
                        );
                    }

                    context.SaveChanges ();
                } catch (Exception ex) {
                    var logger = services.GetRequiredService<ILogger<Program>> ();
                    logger.LogError ("An error occured seeding database.", ex);
                }
            }

            host.Run ();
        }

        public static IHostBuilder CreateHostBuilder (string[] args) =>
            Host.CreateDefaultBuilder (args)
            .ConfigureWebHostDefaults (webBuilder => {
                webBuilder.UseStartup<Startup> ();
            });
    }
}