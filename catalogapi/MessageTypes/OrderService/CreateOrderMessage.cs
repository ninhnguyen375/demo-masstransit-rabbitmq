using System.Collections.Generic;

namespace MessageTypes.OrderService {
    public class CreateOrderMessage {
        public int Id { get; set; }
        public double TotalPrice { get; set; }
        public virtual ICollection<OrderDetailDTO> OrderDetail { get; set; }
    }

    public class OrderDetailDTO {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public double ProductPrice { get; set; }
        public double TotalPrice { get; set; }
        public int Quantity { get; set; }
    }
}