using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using catalogapi;
using GreenPipes;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using orderapi.Consumers;
using orderapi.Infrastructure;
using orderapi.Models;

namespace orderapi {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.AddHealthChecks ();
            services.AddControllers ();
            services.AddDbContext<AppDbContext> (config => {
                config.UseSqlite ("Data Source=./App.db");
            });
            services.AddAutoMapper (typeof (MappingProfile));

            services.AddMassTransit (x => {
                x.AddConsumer<ListProductConsumer> ();
                x.AddBus (context => Bus.Factory.CreateUsingRabbitMq (cfg => {
                    cfg.Host ("rabbitmq://localhost", h => { h.Username ("guest"); h.Password ("guest"); });

                    cfg.ReceiveEndpoint ("order-api", ep => {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry (r => r.Interval (2, 100));
                        ep.ConfigureConsumer<ListProductConsumer> (context);
                    });
                }));
            });

            services.AddHostedService<BusService> ();

            services.AddMassTransitHostedService ();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }

            app.UseHttpsRedirection ();

            app.UseRouting ();

            app.UseAuthorization ();

            app.UseEndpoints (endpoints => {
                endpoints.MapControllers ();
            });
        }
    }
}