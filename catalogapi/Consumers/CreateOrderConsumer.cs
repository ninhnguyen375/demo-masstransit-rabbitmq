using System;
using System.Linq;
using System.Threading.Tasks;
using catalogapi.Models;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using MessageTypes.OrderService;

namespace catalogapi.Consumers {
    public class CreateOrderConsumer : IConsumer<CreateOrderMessage> {
        readonly AppDbContext _context;
        public CreateOrderConsumer (AppDbContext context) {
            _context = context;
        }

        public async Task Consume (ConsumeContext<CreateOrderMessage> context) {
            foreach (var orderDetailItem in context.Message.OrderDetail) {
                // Validate product Quantity
                var currentProduct = await _context.Products.FindAsync (orderDetailItem.ProductId);
                int newQuantity = currentProduct.Quantity - orderDetailItem.Quantity;
                if (newQuantity < 0) {
                    throw new Exception ("Quantity must be greater than zero");
                } else {
                    currentProduct.Quantity = newQuantity;
                }
            }

            await _context.SaveChangesAsync();
        }
    }
}