using System.Collections.Generic;

namespace orderapi.DTOs.EntityDTO {
    public class OrderDTO {
        public int Id { get; set; }
        public double TotalPrice { get; set; }
        public virtual IEnumerable<OrderDetailDTO> OrderDetail { get; set; }
    }
}