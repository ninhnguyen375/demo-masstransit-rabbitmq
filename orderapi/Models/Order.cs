using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace orderapi.Models {
    public class Order {
        [Key]
        public int Id { get; set; }
        public double TotalPrice { get; set; }
        public virtual ICollection<OrderDetail> OrderDetail { get; set; }
    }
}