using System.Threading.Tasks;
using catalogapi.Models;
using MassTransit;
using MessageTypes.CatalogService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace catalogapi.Controllers {
    [ApiController]
    [Route ("[controller]")]
    public class ProductController : ControllerBase {
        private readonly AppDbContext _context;
        private readonly IBus _bus;

        public ProductController (AppDbContext context, IBus bus) {
            _context = context;
            _bus = bus;
        }

        [HttpGet]
        public async Task<ActionResult> GetProducts () {
            var products = await _context.Products.ToListAsync ();

            await _bus.Publish (new ListProductMessage {
                Id = 1, Message = "[✔] Message from: catalog > GetProducts"
            });
            return Ok (products);
        }

    }
}