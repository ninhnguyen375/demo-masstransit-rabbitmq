using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using catalogapi.Consumers;
using catalogapi.Models;
using GreenPipes;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MessageTypes.OrderService;

namespace catalogapi {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.AddControllers ();
            // Consumer dependencies should be scoped
            services.AddScoped<CreateOrderConsumer> ();
            
            services.AddDbContext<AppDbContext> (config => {
                config.UseSqlite ("Data Source=./App.db");
            });

            services.AddMassTransit (x => {
                x.AddConsumer<CreateOrderConsumer> ();
                x.AddBus (context => Bus.Factory.CreateUsingRabbitMq (cfg => {
                    cfg.Host ("rabbitmq://localhost", h => { h.Username ("guest"); h.Password ("guest"); });

                    cfg.ReceiveEndpoint ("catalog-api", ep => {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry (r => r.Interval (2, 100));
                        ep.ConfigureConsumer<CreateOrderConsumer> (context);
                    });
                }));
            });

            services.AddHostedService<BusService> ();

            services.AddMassTransitHostedService ();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }

            app.UseHttpsRedirection ();

            app.UseRouting ();

            app.UseAuthorization ();

            app.UseEndpoints (endpoints => {
                endpoints.MapControllers ();
            });
        }
    }
}