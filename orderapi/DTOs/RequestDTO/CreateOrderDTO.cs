using System.Collections.Generic;

namespace orderapi.DTOs.RequestDTO
{
    public class CreateOrderDTO
    {
        public IEnumerable<CartItem> CartItems { get; set; }
    }

    public class CartItem {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}