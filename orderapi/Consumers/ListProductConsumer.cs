using System;
using System.Threading.Tasks;
using catalogapi;
using MassTransit;
using MessageTypes.CatalogService;

namespace orderapi.Consumers {
    public class ListProductConsumer : IConsumer<ListProductMessage>
    {
        public Task Consume(ConsumeContext<ListProductMessage> context)
        {
            Console.WriteLine($"{context.Message.Message}");
            return Task.CompletedTask;
        }
    }
}